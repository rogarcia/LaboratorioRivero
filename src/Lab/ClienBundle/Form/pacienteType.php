<?php

namespace Lab\ClienBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class pacienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('cedula')
            ->add('telefono')
            ->add('sexo', 'choice', array('choices' => 
            array('SexoM' => 'Masculino', 'SexoF' => 'Femenino'),'placeholder' => 'seleccionar sexo'))
            ->add('direccion')
            ->add('fechaRegistroAt')
            ->add('Registrar', 'submit', array('label' => 'Registrar Paciente'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lab\ClienBundle\Entity\paciente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lab_clienbundle_paciente';
    }
}
