<?php

namespace Lab\ClienBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Lab\ClienBundle\Entity\paciente;
use Lab\ClienBundle\Form\pacienteType;
use Symfony\Component\HttpFoundation\Session\Session;

class PastController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pas = $em->getRepository('LabClienBundle:paciente')->findAll();
/*
        $res = 'Lista de Usuario : <br />';

        foreach($Past as $pas) {
        	$res .= 'Nombre <br />' . $pas->getNombre() . '<br /> Apellido <br />' . $pas->getApellido() . '<br /> Cedula <br />' . $pas->getCedula();
        }
        return new Response($res);*/
        return $this->render('LabClienBundle:vista:index.html.twig', 
            array('pas' => $pas));
    }
    public function addAction()
    {
        $pas = new paciente();

        $form = $this->createCreateForm($pas);

        return $this->render('LabClienBundle:vista:add.html.twig', 
            array('form' => $form->createView()));
    }
    private function createCreateForm(paciente $entity)
    {
        $form = $this->createForm(new pacienteType(), $entity, 
            array('action' => $this->generateUrl('lab_clien_create'),
                'method' => 'POST'
                ));
        return $form;
    }
    public function createAction(Request $request)
    {
        $pas = new paciente();
        $form = $this->createCreateForm($pas);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pas);
            $em->flush();


            $this->addFlash("mensaje", "ElPaciente ha Sido Creado");

            return $this->redirectToRoute('lab_clien_index');
        }
        return $this->render('LabClienBundle:vista:add.html.twig', 
            array('form' => $form->createView()));
    }

    public function verAction($id)
    {
    $repository = $this->getDoctrine()->getRepository('LabClienBundle:paciente');

    $pas = $repository->find($id);

    	return new Response('Nombre <br />' . $pas->getNombre() . '<br /> Apellido <br />' . $pas->getApellido() . '<br /> Cedula <br />' . $pas->getCedula());

    }
}
