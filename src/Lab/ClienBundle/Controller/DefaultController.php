<?php

namespace Lab\ClienBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LabClienBundle:Default:index.html.twig', array('name' => $name));
    }
}
